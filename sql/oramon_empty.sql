-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Май 15 2016 г., 14:43
-- Версия сервера: 5.5.47-MariaDB
-- Версия PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `oramon_empty`
--
CREATE DATABASE IF NOT EXISTS `oramon` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `oramon`;

-- --------------------------------------------------------

--
-- Структура таблицы `alerts`
--

DROP TABLE IF EXISTS `alerts`;
CREATE TABLE IF NOT EXISTS `alerts` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(2000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `alerts`
--

INSERT INTO `alerts` (`id`, `name`, `description`) VALUES
(1, 'Новый OS_USER', 'Такой пользователь первый раз подключается к БД'),
(2, 'Новое место подключения', 'Пользователь первый раз подключается к БД с этого хоста'),
(3, 'Новый ORACLE_USER', 'Эта учетная запись Oracle никогда не использовалась.'),
(4, 'С возвращением!', 'OS_USER подключился после отсутствия более 5 дней, но менее 21 дня.'),
(5, 'Давно не виделись.', 'OS_USER последний раз подключался к БД как минимум три недели назад.'),
(6, 'Прыг-скок', 'OS_USER сменил хост, с которого подключается к БД'),
(7, 'Четыре руки!', 'OS_USER подключился к БД более чем с одного хоста.'),
(8, 'Кое-кто давно не пользовался этим ORACLE_USER', 'Этот OS_USER больше трех недель не пользовался этой учеткой в Oracle'),
(9, 'Этим логином Oracle давно не пользовались.', 'Последнее использование ORACLE_USER было более трех недель назад.'),
(10, 'Использован новый ORACLE_USER', 'Кое-кто впервые воспользовался этой учетной записью Oracle'),
(11, 'Новая роль ORACLE', 'Эта роль раньше никогда не использовалась'),
(13, 'Использована новая роль ORACLE', 'Кое-кто впервые воспользовался этой ролью ORACLE'),
(14, 'Этой ролью ORACLE давно не пользовались', 'Последний раз эта роль ORACLE использовалась больше трех недель назад'),
(15, 'Кое-кто давно не использовал эту роль ORACLE', 'Последний раз эта роль ORACLE использовалась больше трех недель назад данным пользователем'),
(16, 'Ошибка при подключении к БД', 'logon failure'),
(17, 'Ошибка при назначении роли', 'SET ROLE failure'),
(18, 'Много ошибок', 'Больше десяти ошибок в базе для этой пары userhost'),
(19, 'Очень много ошибок!', 'Больше сотни ошибок в базе для этой пары userhost'),
(20, 'ALTER SYSTEM', 'Использование привилегий '),
(21, 'Ошибка ALTER SYSTEM', 'Ошибка при использовании привилегий'),
(22, 'ALTER USER', 'Использование привилегий. Изменение ORACLE USER'),
(23, 'Ошибка ALTER USER', 'Ошибка при использовании привилегий. Изменение ORACLE USER'),
(24, 'Создан новый ORACLE USER', ''),
(25, 'Ошибка при создании нового ORACLE USER', ''),
(26, 'Назначена роль ORACLE', ''),
(27, 'Ошибка при назначении роли ORACLE', ''),
(28, 'Удаление объекта Oracle', ''),
(29, 'Ошибка удаления объекта Oracle', ''),
(30, 'Отозвана роль ORACLE', ''),
(31, 'Ошибка при отзыве роли ORACLE', '');

-- --------------------------------------------------------

--
-- Структура таблицы `alert_log`
--

DROP TABLE IF EXISTS `alert_log`;
CREATE TABLE IF NOT EXISTS `alert_log` (
  `id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `alert_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `host_id` int(11) NOT NULL,
  `oracle_user_id` int(11) NOT NULL DEFAULT '0',
  `oracle_role_id` int(11) NOT NULL DEFAULT '0',
  `message` varchar(2000) NOT NULL DEFAULT '',
  `raw_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `connections`
--

DROP TABLE IF EXISTS `connections`;
CREATE TABLE IF NOT EXISTS `connections` (
  `id` int(4) NOT NULL,
  `user` varchar(16) DEFAULT NULL,
  `passwd` varchar(16) DEFAULT NULL,
  `tns_name` varchar(32) DEFAULT NULL,
  `bookmark_type` enum('num','time') NOT NULL DEFAULT 'time',
  `bookmark_num` int(11) DEFAULT NULL,
  `bookmark_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `failures`
--
DROP VIEW IF EXISTS `failures`;
CREATE TABLE IF NOT EXISTS `failures` (
`id` int(11)
,`timestamp` timestamp
,`uname` varchar(40)
,`host` varchar(40)
,`message` varchar(256)
,`raw_id` int(11)
);

-- --------------------------------------------------------

--
-- Структура таблицы `hosts`
--

DROP TABLE IF EXISTS `hosts`;
CREATE TABLE IF NOT EXISTS `hosts` (
  `id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `last_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `last_day_report`
--
DROP VIEW IF EXISTS `last_day_report`;
CREATE TABLE IF NOT EXISTS `last_day_report` (
`id` int(11)
,`timestamp` timestamp
,`alert_code` int(11)
,`alert` varchar(128)
,`uname` varchar(40)
,`host` varchar(40)
,`host_ip` text
,`ora_user` varchar(40)
,`ora_role` varchar(40)
,`message` varchar(2000)
,`raw_id` int(11)
);

-- --------------------------------------------------------

--
-- Структура таблицы `link_userhost_to_failures`
--

DROP TABLE IF EXISTS `link_userhost_to_failures`;
CREATE TABLE IF NOT EXISTS `link_userhost_to_failures` (
  `id` int(11) NOT NULL,
  `userhost_id` int(11) NOT NULL,
  `message` varchar(256) NOT NULL,
  `raw_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `link_user_to_host`
--

DROP TABLE IF EXISTS `link_user_to_host`;
CREATE TABLE IF NOT EXISTS `link_user_to_host` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `host_id` int(11) NOT NULL,
  `status` enum('online','offline') NOT NULL DEFAULT 'offline',
  `failures_count` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `link_user_to_oracle_role`
--

DROP TABLE IF EXISTS `link_user_to_oracle_role`;
CREATE TABLE IF NOT EXISTS `link_user_to_oracle_role` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `oracle_role_id` int(11) NOT NULL,
  `last_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `link_user_to_oracle_user`
--

DROP TABLE IF EXISTS `link_user_to_oracle_user`;
CREATE TABLE IF NOT EXISTS `link_user_to_oracle_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `oracle_user_id` int(11) NOT NULL,
  `last_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oracle_roles`
--

DROP TABLE IF EXISTS `oracle_roles`;
CREATE TABLE IF NOT EXISTS `oracle_roles` (
  `id` int(11) NOT NULL,
  `conn_id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `last_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oracle_users`
--

DROP TABLE IF EXISTS `oracle_users`;
CREATE TABLE IF NOT EXISTS `oracle_users` (
  `id` int(11) NOT NULL,
  `conn_id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `last_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `ossim_log`
--
DROP VIEW IF EXISTS `ossim_log`;
CREATE TABLE IF NOT EXISTS `ossim_log` (
`id` int(11)
,`timestamp` timestamp
,`alert_code` int(11)
,`alert` varchar(128)
,`uname` varchar(40)
,`host` varchar(40)
,`host_ip` text
,`ora_user` varchar(40)
,`ora_role` varchar(40)
,`message` varchar(2000)
,`raw_id` int(11)
,`tns` varchar(32)
);

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `os_ora_users`
--
DROP VIEW IF EXISTS `os_ora_users`;
CREATE TABLE IF NOT EXISTS `os_ora_users` (
`id` int(11)
,`uname` varchar(40)
,`tns` varchar(32)
,`ora_name` varchar(40)
,`last_time` timestamp
);

-- --------------------------------------------------------

--
-- Структура таблицы `os_users`
--

DROP TABLE IF EXISTS `os_users`;
CREATE TABLE IF NOT EXISTS `os_users` (
  `id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `status` enum('online','offline') NOT NULL DEFAULT 'offline',
  `last_logon_host_id` int(11) DEFAULT NULL,
  `last_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `raw`
--

DROP TABLE IF EXISTS `raw`;
CREATE TABLE IF NOT EXISTS `raw` (
  `id` int(11) NOT NULL,
  `source_id` int(4) NOT NULL,
  `OS_USERNAME` varchar(255) DEFAULT NULL,
  `USERNAME` varchar(30) DEFAULT NULL,
  `USERHOST` varchar(128) DEFAULT NULL,
  `TERMINAL` varchar(255) DEFAULT NULL,
  `TIMESTAMP` timestamp NULL DEFAULT NULL,
  `OWNER` varchar(30) DEFAULT NULL,
  `OBJ_NAME` varchar(128) DEFAULT NULL,
  `ACTION` int(11) DEFAULT NULL,
  `ACTION_NAME` varchar(28) DEFAULT NULL,
  `NEW_OWNER` varchar(30) DEFAULT NULL,
  `NEW_NAME` varchar(128) DEFAULT NULL,
  `OBJ_PRIVILEGE` varchar(16) DEFAULT NULL,
  `SYS_PRIVILEGE` varchar(40) DEFAULT NULL,
  `ADMIN_OPTION` varchar(1) DEFAULT NULL,
  `GRANTEE` varchar(30) DEFAULT NULL,
  `AUDIT_OPTION` varchar(40) DEFAULT NULL,
  `SES_ACTIONS` varchar(19) DEFAULT NULL,
  `LOGOFF_TIME` timestamp NULL DEFAULT NULL,
  `LOGOFF_LREAD` int(11) DEFAULT NULL,
  `LOGOFF_PREAD` int(11) DEFAULT NULL,
  `LOGOFF_LWRITE` int(11) DEFAULT NULL,
  `LOGOFF_DLOCK` varchar(40) DEFAULT NULL,
  `COMMENT_TEXT` varchar(4000) DEFAULT NULL,
  `SESSIONID` int(11) DEFAULT NULL,
  `ENTRYID` int(11) DEFAULT NULL,
  `STATEMENTID` int(11) DEFAULT NULL,
  `RETURNCODE` int(11) DEFAULT NULL,
  `PRIV_USED` varchar(40) DEFAULT NULL,
  `CLIENT_ID` varchar(64) DEFAULT NULL,
  `ECONTEXT_ID` varchar(64) DEFAULT NULL,
  `SESSION_CPU` int(11) DEFAULT NULL,
  `EXTENDED_TIMESTAMP` timestamp NULL DEFAULT NULL,
  `PROXY_SESSIONID` int(11) DEFAULT NULL,
  `GLOBAL_UID` varchar(32) DEFAULT NULL,
  `INSTANCE_NUMBER` int(11) DEFAULT NULL,
  `OS_PROCESS` varchar(16) DEFAULT NULL,
  `TRANSACTIONID` bit(64) DEFAULT NULL,
  `SCN` bigint(20) DEFAULT NULL,
  `SQL_BIND` varchar(2000) DEFAULT NULL,
  `SQL_TEXT` varchar(2000) DEFAULT NULL,
  `OBJ_EDITION_NAME` varchar(30) DEFAULT NULL,
  `DBID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
  `id` int(11) NOT NULL,
  `link_user_to_host_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `userhost`
--
DROP VIEW IF EXISTS `userhost`;
CREATE TABLE IF NOT EXISTS `userhost` (
`id` int(11)
,`uname` varchar(40)
,`host` varchar(40)
,`status` enum('online','offline')
,`failures_count` int(11)
);

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `user_role`
--
DROP VIEW IF EXISTS `user_role`;
CREATE TABLE IF NOT EXISTS `user_role` (
`id` int(11)
,`uname` varchar(40)
,`tns` varchar(32)
,`ora_role` varchar(40)
,`last_time` timestamp
);

-- --------------------------------------------------------

--
-- Структура для представления `failures`
--
DROP TABLE IF EXISTS `failures`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `failures` AS select `f`.`id` AS `id`,`raw`.`TIMESTAMP` AS `timestamp`,`u`.`name` AS `uname`,`h`.`name` AS `host`,`f`.`message` AS `message`,`f`.`raw_id` AS `raw_id` from ((((`link_userhost_to_failures` `f` join `link_user_to_host` `uh` on((`f`.`userhost_id` = `uh`.`id`))) join `os_users` `u` on((`uh`.`user_id` = `u`.`id`))) join `hosts` `h` on((`uh`.`host_id` = `h`.`id`))) join `raw` on((`f`.`raw_id` = `raw`.`id`)));

-- --------------------------------------------------------

--
-- Структура для представления `last_day_report`
--
DROP TABLE IF EXISTS `last_day_report`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `last_day_report` AS select `ossim_log`.`id` AS `id`,`ossim_log`.`timestamp` AS `timestamp`,`ossim_log`.`alert_code` AS `alert_code`,`ossim_log`.`alert` AS `alert`,`ossim_log`.`uname` AS `uname`,`ossim_log`.`host` AS `host`,`ossim_log`.`host_ip` AS `host_ip`,`ossim_log`.`ora_user` AS `ora_user`,`ossim_log`.`ora_role` AS `ora_role`,`ossim_log`.`message` AS `message`,`ossim_log`.`raw_id` AS `raw_id` from `ossim_log` where ((`ossim_log`.`timestamp` >= timestamp((curdate() - interval 1 day),'09:00:00')) and (`ossim_log`.`timestamp` < timestamp(curdate(),'09:00:00')) and (((`ossim_log`.`alert_code` <> 7) and (`ossim_log`.`alert_code` <> 6)) or (`ossim_log`.`uname` <> 'tb-user'))) order by `ossim_log`.`id`;

-- --------------------------------------------------------

--
-- Структура для представления `ossim_log`
--
DROP TABLE IF EXISTS `ossim_log`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ossim_log` AS select `l`.`id` AS `id`,`l`.`timestamp` AS `timestamp`,`a`.`id` AS `alert_code`,`a`.`name` AS `alert`,`u`.`name` AS `uname`,`h`.`name` AS `host`,substring_index(substring_index(`raw`.`COMMENT_TEXT`,'HOST=',-(1)),')',1) AS `host_ip`,`ou`.`name` AS `ora_user`,`r`.`name` AS `ora_role`,`l`.`message` AS `message`,`l`.`raw_id` AS `raw_id`,`c`.`tns_name` AS `tns` from (((((((`alert_log` `l` join `alerts` `a` on((`l`.`alert_id` = `a`.`id`))) join `os_users` `u` on((`l`.`user_id` = `u`.`id`))) join `hosts` `h` on((`l`.`host_id` = `h`.`id`))) left join `oracle_users` `ou` on((`l`.`oracle_user_id` = `ou`.`id`))) left join `oracle_roles` `r` on((`l`.`oracle_role_id` = `r`.`id`))) join `raw` on((`l`.`raw_id` = `raw`.`id`))) join `connections` `c` on((`raw`.`source_id` = `c`.`id`))) order by `l`.`id` desc;

-- --------------------------------------------------------

--
-- Структура для представления `os_ora_users`
--
DROP TABLE IF EXISTS `os_ora_users`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `os_ora_users` AS select `uo`.`id` AS `id`,`u`.`name` AS `uname`,`c`.`tns_name` AS `tns`,`o`.`name` AS `ora_name`,`uo`.`last_time` AS `last_time` from (((`link_user_to_oracle_user` `uo` join `os_users` `u` on((`uo`.`user_id` = `u`.`id`))) join `oracle_users` `o` on((`uo`.`oracle_user_id` = `o`.`id`))) join `connections` `c` on((`o`.`conn_id` = `c`.`id`))) order by `u`.`name`,`c`.`tns_name`,`o`.`name`;

-- --------------------------------------------------------

--
-- Структура для представления `userhost`
--
DROP TABLE IF EXISTS `userhost`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `userhost` AS select `uh`.`id` AS `id`,`u`.`name` AS `uname`,`h`.`name` AS `host`,`uh`.`status` AS `status`,`uh`.`failures_count` AS `failures_count` from ((`link_user_to_host` `uh` join `os_users` `u` on((`uh`.`user_id` = `u`.`id`))) join `hosts` `h` on((`uh`.`host_id` = `h`.`id`))) order by `u`.`name`,`h`.`name`;

-- --------------------------------------------------------

--
-- Структура для представления `user_role`
--
DROP TABLE IF EXISTS `user_role`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `user_role` AS select `lor`.`id` AS `id`,`u`.`name` AS `uname`,`c`.`tns_name` AS `tns`,`r`.`name` AS `ora_role`,`lor`.`last_time` AS `last_time` from (((`link_user_to_oracle_role` `lor` join `os_users` `u` on((`lor`.`user_id` = `u`.`id`))) join `oracle_roles` `r` on((`lor`.`oracle_role_id` = `r`.`id`))) join `connections` `c` on((`r`.`conn_id` = `c`.`id`))) order by `u`.`name`,`c`.`tns_name`,`r`.`name`;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `alerts`
--
ALTER TABLE `alerts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `alert_log`
--
ALTER TABLE `alert_log`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `connections`
--
ALTER TABLE `connections`
  ADD UNIQUE KEY `id` (`id`);

--
-- Индексы таблицы `hosts`
--
ALTER TABLE `hosts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `link_userhost_to_failures`
--
ALTER TABLE `link_userhost_to_failures`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `link_user_to_host`
--
ALTER TABLE `link_user_to_host`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `link_user_to_oracle_role`
--
ALTER TABLE `link_user_to_oracle_role`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `link_user_to_oracle_user`
--
ALTER TABLE `link_user_to_oracle_user`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `oracle_roles`
--
ALTER TABLE `oracle_roles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `oracle_users`
--
ALTER TABLE `oracle_users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `os_users`
--
ALTER TABLE `os_users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `raw`
--
ALTER TABLE `raw`
  ADD UNIQUE KEY `id` (`id`);

--
-- Индексы таблицы `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `alerts`
--
ALTER TABLE `alerts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT для таблицы `alert_log`
--
ALTER TABLE `alert_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `connections`
--
ALTER TABLE `connections`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `hosts`
--
ALTER TABLE `hosts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `link_userhost_to_failures`
--
ALTER TABLE `link_userhost_to_failures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `link_user_to_host`
--
ALTER TABLE `link_user_to_host`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `link_user_to_oracle_role`
--
ALTER TABLE `link_user_to_oracle_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `link_user_to_oracle_user`
--
ALTER TABLE `link_user_to_oracle_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oracle_roles`
--
ALTER TABLE `oracle_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oracle_users`
--
ALTER TABLE `oracle_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `os_users`
--
ALTER TABLE `os_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `raw`
--
ALTER TABLE `raw`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
