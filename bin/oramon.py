#! /usr/bin/python
# -*- coding: utf8 -*-
# версия 0.1.0 

import os
import sys
import datetime
import cx_Oracle
import MySQLdb
import codecs

# ---- init ----

(dbhost, dbuser, dbpass, dbschema, dbcharset)=('localhost', 'oramon', 'your_oramon_password', 'oramon', 'utf8') # MySQL connection params

select_from_ora = 'SELECT '\
    'OS_USERNAME,USERNAME,USERHOST,TERMINAL,TIMESTAMP,OWNER,OBJ_NAME,ACTION,ACTION_NAME,'\
    'NEW_OWNER,NEW_NAME,OBJ_PRIVILEGE,SYS_PRIVILEGE,ADMIN_OPTION,GRANTEE,AUDIT_OPTION,'\
    'SES_ACTIONS,LOGOFF_TIME,LOGOFF_LREAD,LOGOFF_PREAD,LOGOFF_LWRITE,LOGOFF_DLOCK,COMMENT_TEXT,'\
    'SESSIONID,ENTRYID,STATEMENTID,RETURNCODE,PRIV_USED,CLIENT_ID,ECONTEXT_ID,SESSION_CPU,'\
    'EXTENDED_TIMESTAMP,PROXY_SESSIONID,GLOBAL_UID,INSTANCE_NUMBER,OS_PROCESS,TRANSACTIONID,'\
    'SCN,SQL_BIND,SQL_TEXT,OBJ_EDITION_NAME,DBID'
    
insert_to_raw = 'INSERT INTO raw(source_id,'\
    'OS_USERNAME,USERNAME,USERHOST,TERMINAL,TIMESTAMP,OWNER,OBJ_NAME,ACTION,ACTION_NAME,'\
    'NEW_OWNER,NEW_NAME,OBJ_PRIVILEGE,SYS_PRIVILEGE,ADMIN_OPTION,GRANTEE,AUDIT_OPTION,'\
    'SES_ACTIONS,LOGOFF_TIME,LOGOFF_LREAD,LOGOFF_PREAD,LOGOFF_LWRITE,LOGOFF_DLOCK,COMMENT_TEXT,'\
    'SESSIONID,ENTRYID,STATEMENTID,RETURNCODE,PRIV_USED,CLIENT_ID,ECONTEXT_ID,SESSION_CPU,'\
    'EXTENDED_TIMESTAMP,PROXY_SESSIONID,GLOBAL_UID,INSTANCE_NUMBER,OS_PROCESS,TRANSACTIONID,'\
    'SCN,SQL_BIND,SQL_TEXT,OBJ_EDITION_NAME,DBID) '\
    'VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'

RETURN_CODES = {
    911: u'Invalid Character.',
    988: u'Missing or invalid password(s).',
    1004: u'Logon denied.',
    1005: u'Null Password',
    1017: u'Invalid username/password.',
    1031: u'No Privilege',
    1045: u'User string lacks CREATE SESSION privilege; logon denied.',
    1918: u'No Such UserID',
    1920: u'No Such Role',
    1924: u'Role not granted or does not exist.',
    1951: u'The role you tried to revoke was not granted to the user.', 
    9911: u'Incorrect user password.',
    28000: u'The account is locked.',
    28001: u'Your password has expired.',
    28002: u'Your account will expire soon. Сhange your password now.',
    28003: u'The password is not complex enough.',
    28007: u'Password cannot be reused.',
    28008: u'Invalid old password.',
    28009: u'Connection to sys should be as sysdba or sysoper.',
    28011: u'Your account will expire soon. Сhange your password now.',
    28221: u'The original password was not supplied.'
}

# Эти переменные используются как глобальные:
# conn_id, tns, raw_id, row, connToMySQL, cursor_my
#
# ------ end of init -----

def alert (timestamp,alert_id=0,user_id=0,host_id=0,ora_user_id=0, ora_role_id=0, message="") :
    insert = "insert INTO alert_log(timestamp, alert_id, user_id, host_id, oracle_user_id, oracle_role_id, message, raw_id) VALUES(%s,%s,%s,%s,%s,%s,%s,%s)"
    cursor_my.execute(insert,(timestamp,alert_id,user_id,host_id,ora_user_id,ora_role_id,message,raw_id))
    connToMySQL.commit()
    return

def get_user_and_host ():
    # Ищем пользователя (OS_USER) в базе
    user_name = row[0].strip().strip('\0')
    select = "select id, name, status, last_time from os_users where name = %s"
    cursor_my.execute(select,(user_name,))
    user_row = cursor_my.fetchone()
    if user_row :
        user_id = user_row[0]
    else:
        insert = "insert INTO os_users(name,last_logon_host_id,last_time) VALUES(%s,%s,%s)"
        cursor_my.execute(insert,(user_name,0,row[31]))
        connToMySQL.commit()
        user_id = cursor_my.lastrowid
        alert (row[31], 1, user_id)
    
    if row[2] is None:
        host_name = 'unknown_host'
    elif '\\' in row[2]:
        host_name = row[2].split('\\')[1].lower().strip().strip('\0')
    else:
        host_name = row[2].strip().strip('\0')
    select = "select id from hosts where name like '" + host_name + "'"
    cursor_my.execute(select)
    host_row = cursor_my.fetchone()
    if host_row :
        # Нашли. Обновляем время последнего обнаружения хоста
        host_id = host_row[0]
        update = "update hosts set last_time = %s where id = %s"
        cursor_my.execute(update,(row[31],host_row[0]))
        connToMySQL.commit()
    else:
        insert = "insert INTO hosts(name,last_time) VALUES(%s,%s)"
        cursor_my.execute(insert,(host_name,row[31]))
        connToMySQL.commit()
        host_id = cursor_my.lastrowid
        alert (row[31], 2, user_id, host_id) # Новое место подключения
    # Действия с линком user to host. Ищем линк в базе
    select = "select id from link_user_to_host where user_id = %s and host_id = %s"
    cursor_my.execute(select,(user_id,host_id))
    user_host_row = cursor_my.fetchone()
    if user_host_row :
        link_user_to_host_id = user_host_row[0]        
    else:
        # Не нашли, создаем линк.
        insert = "insert INTO link_user_to_host (user_id,host_id,status,failures_count) VALUES(%s,%s,%s,%s)"
        cursor_my.execute(insert,(user_id,host_id,'offline',0))
        connToMySQL.commit()
        link_user_to_host_id = cursor_my.lastrowid
    return (user_name, host_name, user_id, host_id, link_user_to_host_id)
    
def get_ora_user (user_id=0, host_id=0):
    ora_user_name = row[1].strip().strip('\0')
    select = "select id, conn_id, name, last_time from oracle_users where name = %s and conn_id = %s" 
    cursor_my.execute(select,(ora_user_name,conn_id))
    oracle_users_row = cursor_my.fetchone()
    if oracle_users_row :
        ora_user_id = oracle_users_row[0]
        dd = (row[31] - oracle_users_row[3]).days
        if dd > 21:
             alert (row[31],9,user_id,host_id,ora_user_id) # Этой учеткой ORACLE давно не пользовались
        # обновляем время последнего использования        
        update = "update oracle_users set last_time = %s where id = %s"
        cursor_my.execute(update,(row[31],ora_user_id))
        connToMySQL.commit()
    else:
        # Новый ORACLE_USER
        insert = "insert INTO oracle_users(conn_id,name,last_time) VALUES(%s,%s,%s)"
        cursor_my.execute(insert,(conn_id,ora_user_name,row[31]))
        connToMySQL.commit()
        ora_user_id = cursor_my.lastrowid
        alert (row[31],3,user_id,host_id,ora_user_id) # Новый ORACLE_USER
    return (ora_user_name,ora_user_id)
    
def get_ora_role (user_id=0, host_id=0, ora_user_id=0):
    role_name = row[6].strip().strip('\0')
    # проверяем есть ли такая роль в нашей базе
    select = "select id, conn_id, name, last_time from oracle_roles where name = %s"
    cursor_my.execute(select,(role_name,))
    role_row = cursor_my.fetchone()
    if role_row:
        ora_role_id = role_row[0]
        # Нашли, смотрим когда ее использовали последний раз
        dd = (row[31] - role_row[3]).days
        if dd > 21:
            alert (row[31],14,user_id,host_id,ora_user_id,ora_role_id) # Этой ролью давно не пользовались
        # обновляем время последнего использования        
        update = "update oracle_roles set last_time = %s where id = %s"
        cursor_my.execute(update,(row[31],ora_role_id))
        connToMySQL.commit()
    else:
        # Записываем в базу новую роль
        insert = "insert INTO oracle_roles(conn_id,name,last_time) VALUES(%s,%s,%s)"
        cursor_my.execute(insert,(conn_id,role_name,row[31]))
        connToMySQL.commit()
        ora_role_id = cursor_my.lastrowid
        alert (row[31],11,user_id,host_id,ora_user_id,ora_role_id) # Новая роль ORACLE
        
    return (role_name, ora_role_id)
        
def get_link_user_to_ora_user (user_id=0, host_id=0, ora_user_id=0):
    # Ищем в базе линк OS_USER to ORACLE_USER
    select = "select id, user_id, oracle_user_id, last_time from link_user_to_oracle_user where user_id = %s and oracle_user_id = %s"
    cursor_my.execute(select,(user_id,ora_user_id))
    link_users_row = cursor_my.fetchone()
    if link_users_row :
        # Нашли. Обновляем время последнего использования
        link_user_to_ora_user_id = link_users_row[0]
        dd = (row[31] - link_users_row[3]).days
        if dd > 21:
            alert (row[31],8,user_id,host_id,ora_user_id) # Кое-кто давно не пользовался этой учеткой Oracle
        update = "update link_user_to_oracle_user set last_time = %s where id = %s"
        cursor_my.execute(update,(row[31],link_user_to_ora_user_id)) 
        connToMySQL.commit()
    else:
        # Не нашли. Создаем новый линк
        insert = "insert INTO link_user_to_oracle_user(user_id,oracle_user_id,last_time) VALUES(%s,%s,%s)"
        cursor_my.execute(insert,(user_id,ora_user_id,row[31]))
        connToMySQL.commit()
        link_user_to_ora_user_id = cursor_my.lastrowid
        alert (row[31],10,user_id,host_id,ora_user_id) # Использован новый ORACLE_USER
        
    return link_user_to_ora_user_id
    
def get_link_user_to_role (user_id=0, host_id=0, ora_user_id=0, ora_role_id=0):
    # Ищем линк юзер-роль
    select = "select id, user_id, oracle_role_id, last_time from link_user_to_oracle_role where user_id = %s and oracle_role_id = %s"
    cursor_my.execute(select,(user_id,ora_role_id))
    link_user_to_role_row = cursor_my.fetchone()
    if link_user_to_role_row :
        # Нашли. Смотрим время последнего использования
        link_user_to_role_id = link_user_to_role_row[0]
        dd = (row[31] - link_user_to_role_row[3]).days
        if dd > 21:
            alert (row[31],15,user_id,host_id,ora_user_id,ora_role_id) # Кое-кто давно не пользовался этой ролью
        # обновляем время последнего использования        
        update = "update link_user_to_oracle_role set last_time = %s where id = %s"
        cursor_my.execute(update,(row[31],link_user_to_role_id))
        connToMySQL.commit()
    else:
        # Не нашли. Создаем новый линк
        insert = "insert INTO link_user_to_oracle_role(user_id,oracle_role_id,last_time) VALUES(%s,%s,%s)"
        cursor_my.execute(insert,(user_id,ora_role_id,row[31]))
        connToMySQL.commit()
        link_user_to_role_id = cursor_my.lastrowid
        alert (row[31],13,user_id,host_id,ora_user_id,ora_role_id) # Кое-кто использовал новую роль
        
    return link_user_to_role_id
    
def get_existing_IDs ():
    if row[2] is None:
        host_name = u'unknown_host'
    elif '\\' in row[2]:
        host_name = row[2].split('\\')[1].lower().strip().strip('\0')
    else:
        host_name = row[2].strip().strip('\0')
    user_name = row[0].strip().strip('\0')
    ora_user_name = unicode(row[1]).strip().strip('\0')
    ora_object = unicode(row[6]).strip().strip('\0')
    cursor_my.execute("select id from os_users where name=%s", (user_name,))
    my_row = cursor_my.fetchone()
    if my_row:
        user_id = my_row[0]
    else:
        user_id = 0
    cursor_my.execute("select id from hosts where name=%s", (host_name,))
    my_row = cursor_my.fetchone()
    if my_row:
        host_id = my_row[0]
    else: 
        host_id = 0
    cursor_my.execute("select id from oracle_users where name=%s", (ora_user_name,))
    my_row= cursor_my.fetchone()
    if my_row:
        ora_user_id = my_row[0]
    else: 
        ora_user_id = 0
    cursor_my.execute("select id from oracle_roles where name=%s", (ora_object,))
    my_row = cursor_my.fetchone()
    if my_row:
        ora_role_id = my_row[0]
    else: 
        ora_role_id = 0
        
    return (user_name, host_name, ora_user_name, ora_object, user_id, host_id, ora_user_id, ora_role_id)

def reg_failure ():
        
    (user_name, host_name, user_id, host_id, link_user_to_host_id) = get_user_and_host()
    select = "select failures_count from link_user_to_host where id = %s"
    cursor_my.execute(select,(link_user_to_host_id,))
    user_host_row = cursor_my.fetchone()
    failures_count = int(user_host_row[0]) + 1
    update = "update link_user_to_host set failures_count=%s where id=%s"
    cursor_my.execute(update,(failures_count,link_user_to_host_id))
    connToMySQL.commit() 


    # Регистрируем фэйлур в базе
    message = RETURN_CODES.get(row[26],u'Unknown error') + u" with " + tns + u" on action " + row[8]
    if row[1] is not None:
        message += u" on ora_user " + row[1]
    if row[6] is not None:
        message += u" on ora_object " + row[6]
    if row[14] is not None:
        message += u" for ora_user " + row[14]
    insert = "insert INTO link_userhost_to_failures (userhost_id,message,raw_id) VALUES(%s,%s,%s)"
    cursor_my.execute(insert,(link_user_to_host_id,message,raw_id))
    connToMySQL.commit()
    link_userhost_to_failure = cursor_my.lastrowid
    
    if failures_count > 100:
        alert_id = 19
        message1 = u"Проверьте монитор для userhost_id=" + str(link_user_to_host_id) + u" это id пары " + user_name + "::" + host_name
        alert (row[31],alert_id,user_id,host_id,0,0, message1) 
    elif failures_count > 10:
        alert_id = 18
        message1 = u"Проверьте монитор для userhost_id=" + str(link_user_to_host_id) + u" это id пары " + user_name + "::" + host_name
        alert (row[31],alert_id,user_id,host_id,0,0, message1)
        
    return (link_userhost_to_failure,user_id,user_name,host_id,host_name,link_user_to_host_id,message,failures_count)
    
def analyze_logon_failure():

    (link_userhost_to_failure,user_id,user_name,host_id,host_name,link_user_to_host_id,message,failures_count) = reg_failure ()
    alert (row[31],16,user_id,host_id,0,0, message) # logon failure
    
    return
    

    
def analyze_set_role_failure():

    (link_userhost_to_failure,user_id,user_name,host_id,host_name,link_user_to_host_id,message,failures_count) = reg_failure ()
    alert (row[31],17,user_id,host_id,0,0, message) # SET ROLE failure
    
    return
    

    
def analyze_set_role_success():
  
    (user_name, host_name, user_id, host_id, link_user_to_host_id) = get_user_and_host()
    (ora_user_name,ora_user_id) = get_ora_user(user_id,host_id)
    link_user_to_ora_user_id = get_link_user_to_ora_user(user_id, host_id, ora_user_id)
    (role_name, ora_role_id) = get_ora_role(user_id,host_id,ora_user_id)
    link_user_to_role_id = get_link_user_to_role(user_id, host_id, ora_user_id, ora_role_id)
    
    return

def analyze_logoff():

    # Ищем открытую сессию
    select = "select link_user_to_host_id from sessions where session_id = %s"
    cursor_my.execute(select,(row[23],))
    session_row = cursor_my.fetchone()
    if session_row :
        link_user_to_host_id = session_row[0]
        cursor_my.execute("delete from sessions where session_id=%s", (row[23],))
        connToMySQL.commit()
        # проверяем остались ли еще сессии
        select = "select session_id from sessions where link_user_to_host_id = %s"
        cursor_my.execute(select,(link_user_to_host_id,))

        if cursor_my.fetchone() :
            # Да остались. тогда просто уходим
            return
        else:
            # Больше нету. Меняем статус в link_user_to_host
            update = "update link_user_to_host set status = 'offline' where id=%s"
            cursor_my.execute(update,(link_user_to_host_id,))
            connToMySQL.commit()
            # Ищем остались ли еще активные соединения с этим юзером
            select = "select id from os_users where name = %s"
            cursor_my.execute(select, (row[0].strip().strip('\0'),))
            user_row = cursor_my.fetchone()
            user_id = user_row[0]
            select = "select id from link_user_to_host where user_id=%s"
            cursor_my.execute(select, (user_id,))
            if cursor_my.fetchone() :
                # да остались. уходим
                return
            else:
                update = "update os_users set status='offline' where user_id = %s"
                cursor_my.execute(update,(user_id,))
                connToMySQL.commit()
            
    return

def analyze_logon_success ():

    (user_name, host_name, user_id, host_id, link_user_to_host_id) = get_user_and_host()
    (ora_user_name,ora_user_id) = get_ora_user(user_id,host_id)
    link_user_to_ora_user_id = get_link_user_to_ora_user(user_id, host_id, ora_user_id)
    
    update = "update link_user_to_host set status='online' where id=%s"
    cursor_my.execute(update,(link_user_to_host_id,))
    connToMySQL.commit()
    
    # Регистрируем новую сессию для этого юзера на этом хосте
    insert = "insert INTO sessions(link_user_to_host_id,session_id,timestamp) VALUES(%s,%s,%s)"
    cursor_my.execute(insert,(link_user_to_host_id,row[23],row[31]))
    connToMySQL.commit()
    
    # проверка на одновременное подключение с разных хостов или смену хоста
    select = "select t2.name from link_user_to_host as t1 join hosts as t2 on t1.host_id = t2.id where t1.user_id = %s and t1.status = 'online'"
    cursor_my.execute(select,(user_id,))
    myrows=cursor_my.fetchall()
    
    if cursor_my.rowcount > 1:
        host_list = ''
        for myrow in myrows:
            host_list += myrow[0] + ", "
        message = u"Список хостов: " + host_list
        alert (row[31],7,user_id,host_id,ora_user_id,0, message) # четыре руки
    else:
        select = "select t1.last_logon_host_id, t2.name from os_users as t1 join hosts as t2 on t1.last_logon_host_id = t2.id where t1.id = %s"
        cursor_my.execute(select,(user_id,))
        myrow=cursor_my.fetchone()
        
        if myrow :
            old_host_id = myrow[0]
            old_host_name = myrow[1]
            
            if host_id <> old_host_id and old_host_id <> 0 :
                message = u"Переход с хоста " + old_host_name + u" на хост " + host_name
                alert (row[31],6, user_id, host_id, ora_user_id, 0, message) # Прыг - скок
                
    # Устанавливаем last_logon_host_id и прочее
    update = "update os_users set status='online', last_logon_host_id = %s, last_time=%s where id=%s"
    cursor_my.execute(update,(host_id,row[31],user_id))
    connToMySQL.commit()

    return

def analyze_alter_system_failure ():
    (link_userhost_to_failure,user_id,user_name,host_id,host_name,link_user_to_host_id,message,failures_count) = reg_failure ()
    alert (row[31],21,user_id,host_id,0,0, message) # ALTER SYSTEM failure
    return
    
def analyze_alter_system():
    (user_name, host_name, ora_user_name, ora_object, user_id, host_id, ora_user_id, ora_role_id) = get_existing_IDs()
    message = u"ALTER SYSTEM by " + user_name + u" from host " + host_name
    alert (row[31],20,user_id,host_id,ora_user_id,ora_role_id, message) # ALTER SYSTEM
    return
    
def analyze_alter_user_failure():
    (link_userhost_to_failure,user_id,user_name,host_id,host_name,link_user_to_host_id,message,failures_count) = reg_failure ()
    alert (row[31],23,user_id,host_id,0,0, message) # ALTER USER failure
    return
    
def analyze_alter_user():
    (user_name, host_name, ora_user_name, ora_object, user_id, host_id, ora_user_id, ora_role_id) = get_existing_IDs()
    message = u"ALTER USER by " + user_name + u" from host " + host_name + u" with ora_user " + ora_user_name + u" on ora_object " + ora_object
    alert (row[31],22,user_id,host_id,ora_user_id,ora_role_id, message) # ALTER USER
    return
    
def analyze_create_user_failure():
    (link_userhost_to_failure,user_id,user_name,host_id,host_name,link_user_to_host_id,message,failures_count) = reg_failure ()
    alert (row[31],25,user_id,host_id,0,0, message) # Create USER failure
    return
    
def analyze_create_user():
    (user_name, host_name, ora_user_name, ora_object, user_id, host_id, ora_user_id, ora_role_id) = get_existing_IDs()
    message = u"New ora_user created by " + user_name + u" from host " + host_name + u" with ora_user " + ora_user_name + u": new ora_user name is " + ora_object
    alert (row[31],24,user_id,host_id,ora_user_id,ora_role_id, message) # Create user
    return
    
def analyze_grant_role_failure():
    (link_userhost_to_failure,user_id,user_name,host_id,host_name,link_user_to_host_id,message,failures_count) = reg_failure ()
    alert (row[31],27,user_id,host_id,0,0, message) # GRANT ROLE failure
    return
    
def analyze_grant_role():
    (user_name, host_name, ora_user_name, ora_object, user_id, host_id, ora_user_id, ora_role_id) = get_existing_IDs()
    message = u"New ora_role " + ora_object + u" granted for " + unicode(row[14]).strip().strip('\0') + u" by os_user " + user_name + u" from host " + host_name + u" with ora_user " + ora_user_name
    alert (row[31],26,user_id,host_id,ora_user_id,ora_role_id, message) # GRANT ROLE
    return

def analyze_revoke_role():
    (user_name, host_name, ora_user_name, ora_object, user_id, host_id, ora_user_id, ora_role_id) = get_existing_IDs()
    message = u"Revoke ora_role " + ora_object + u" from " + unicode(row[14]).strip().strip('\0') + u" by os_user " + user_name + u" from host " + host_name + u" with ora_user " + ora_user_name
    alert (row[31],30,user_id,host_id,ora_user_id,ora_role_id, message) # Revoke ROLE
    return    
    
def analyze_revoke_role_failure():
    (link_userhost_to_failure,user_id,user_name,host_id,host_name,link_user_to_host_id,message,failures_count) = reg_failure ()
    alert (row[31],31,user_id,host_id,0,0, message) # Revoke ROLE failure
    return
    
def analyze_delete_failure():
    (link_userhost_to_failure,user_id,user_name,host_id,host_name,link_user_to_host_id,message,failures_count) = reg_failure ()
    alert (row[31],29,user_id,host_id,0,0, message) # DELETE failure
    return    
    
def analyze_delete():
    (user_name, host_name, ora_user_name, ora_object, user_id, host_id, ora_user_id, ora_role_id) = get_existing_IDs()
    
    alert_id = 28 # DELETE 
    ora_owner = row[5].strip().strip('\0')
    message = u"Delete object " + ora_object + u" owned by " + ora_owner + u". Operator: " + user_name + u" from host " + host_name + u" with ora_user " + ora_user_name
    alert (row[31],28,host_id,ora_user_id,ora_role_id, message) # Delete ora_object
    return    

# --- Main ----

conn_list = []
connToMySQL = MySQLdb.connect(host=dbhost, user=dbuser, passwd=dbpass, db=dbschema, charset=dbcharset)
cursor_my = connToMySQL.cursor()
cursor_my.execute('select * from connections')
row = cursor_my.fetchone()

while row:

    (conn_id,user,passwd,tns,bm_type,bm_id,bm_time) = (row[0],row[1],row[2],row[3],row[4],row[5],row[6])
    conn_list.append((conn_id,user,passwd,tns,bm_type,bm_id,bm_time))
    row = cursor_my.fetchone()
    
for (conn_id,user,passwd,tns,bm_type,bm_id,bm_time) in conn_list:
  
    where = "WHERE TIMESTAMP > TO_TIMESTAMP('" + bm_time.strftime('%Y-%m-%d %H:%M:%S.%f') + "','YYYY-MM-DD HH24:MI:SS.FF')" 

    conn = cx_Oracle.connect(user + "/" + passwd + "@" + tns)
    cursor = conn.cursor()

    cursor.execute(select_from_ora + " FROM DBA_AUDIT_TRAIL " + where + " ORDER BY EXTENDED_TIMESTAMP")
    tstamp = bm_time
    row = cursor.fetchone() 
    while row:
    
        my_values = (conn_id,)
        my_values +=tuple(row)
        cursor_my.execute(insert_to_raw,my_values)
        connToMySQL.commit()
        raw_id = cursor_my.lastrowid
        
        if int(row[7]) == 100  :      # logon  
            if int(row[26]) == 0 :
                analyze_logon_success()
            else:
                analyze_logon_failure()                
        elif int(row[7]) == 101 or int(row[7]) == 102 :   #logoff        
            analyze_logoff()            
        elif int(row[7]) == 55 :        # set role        
            if int(row[26]) == 0 :
                analyze_set_role_success()
            else:
                analyze_set_role_failure()
        elif int(row[7]) == 49 :
            if int(row[26]) == 0 :
                analyze_alter_system()
            else:
                analyze_alter_system_failure() 
        elif int(row[7]) == 43 :
            if int(row[26]) == 0 :
                analyze_alter_user()
            else:
                analyze_alter_user_failure()
        elif int(row[7]) == 51 :
            if int(row[26]) == 0 :
                analyze_create_user()
            else:
                analyze_create_user_failure()
        elif int(row[7]) == 114 :
            if int(row[26]) == 0 :
                analyze_grant_role()
            else:
                analyze_grant_role_failure()
        elif int(row[7]) == 7 :
            if int(row[26]) == 0 :
                analyze_delete()
            else:
                analyze_delete_failure()
        elif int(row[7]) == 115 :
            if int(row[26]) == 0 :
                analyze_revoke_role()
            else:
                analyze_revoke_role_failure()
        tstamp = row[31]
        row = cursor.fetchone()   
        
    conn.close()
    ststamp = tstamp.strftime('%Y-%m-%d %H:%M:%S.%f')
    exstr = "update connections set bookmark_time = STR_TO_DATE('" + ststamp + "','%Y-%m-%d %H:%i:%S.%f')  where tns_name = '" + tns + "'"

    cursor_my.execute(exstr)
    connToMySQL.commit()
    
connToMySQL.close()