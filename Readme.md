# Монитор журнала аудита ORACLE #
### О текущей версии ###
Это версия 0.1.0. Вполне рабочая бета. Изначальный код версии 0.0.1 полностью переписан и стал в два раза компактнее и, надеюсь, понятнее. Больше событий регистрируется в ossim_log.

### Что это? Зачем это? ###
Всё началось с желания получить на OSSIM более приличный журнал доступа к СУБД ORACLE. Более приличный, чем получается по сислогу. Это значит, что нужно читать его из базы данных, из вьюхи `DBA_AUDIT_TRAIL`. Но, вот беда, database плагины OSSIM не умеют читать из БД ORACLE. Точнее говоря, чтобы это делать, надо прикрутить модуль `cx_Oracle` для питона. А для этого, в свою очередь, надо установить клиента Oracle. 

Тогда мне подумалось, что будет гораздо интереснее и полезнее сделать совсем отдельную систему, которая будет не только забирать и хранить лог, но и производить некоторую собственную обработку событий, а в OSSIM отдавать уже свой собственный лог, содержащий практически полезные алерты. Тем более, что OSSIM и так нагружается очень сильно. В результате появился вот этот ORAMON.

### Принцип работы
Вся система представляет собой базу данных MySQL и скрипт на питоне oramon.py, который читает `DBA_AUDIT_TRAIL` с сервера Oracle, помещает его в таблицу raw, в базу oramon MySQL и делает кое-какой дополнительный анализ событий. Пока анализируются только события LOGON/LOGOFF и SET ROLE. Скрипт отслеживает следующие события:

* Новый пользователь.
* Новый хост (новое место подключения к базе).
* Новая учетная запись Oracle использована для подключения.
* Новая роль использована данным пользователем.
* Одновременное подключение с двух и более хостов (отслеживаются незакрытые сессии).
* Смена пользователем хоста подключения.
* Подключение или использование роли после длительного перерыва (более трех недель)
* Много (более 10) и очень много (более 100) ошибок подключения или установки роли.

Соответственно, информация об истории различных событий накапливается в разных таблицах, где ее можно посмотреть. Для удобства сделаны несколько представлений. Главное называется `ossim_log` - это то, что должен читать плагин OSSIM. А еще есть `failures` - это информация об ошибочных событиях, `userhost` - это информация о том, кто, откуда и когда подключался, и аналогичные представления `os_ora_users` и `user_roles`. 

Подробно всю структуру можно увидеть в файле `oramon_empty.sql`, в [репозитарии этого проекта на bitbucket](https://bitbucket.org/esguardian/oramon/overview).

### Как установить и запустить
Прежде всего потребуется LAMP сервер. На любой системе. Я настоятельно рекомендую установить phpMyAdmin, с ним будет удобнее, это пока очень предварительная версия. Затем необходимо установить на него клиента Oracle и модуль cx_Oracle для питона. Эта операция может оказаться не очень простой. Я делал на виртуалке с RHEL7, пришлось вручную искать пару пакетов для разрешения зависимостей. Инструкции по этому поводу давать нет смысла, поскольку в разных системах делается разными способами.

Важно, когда вы делаете LAMP в конфигурации MySQL прописать принудительную установку кодировки utf8 для клиентских подключений. Это необходимо для OSSIM, который не умеет указывать charset в параметрах соединения. Речь идет вот об этих параметрах:

    [client]
    default-character-set=utf8
    [mysqld]
    skip-character-set-client-handshake
    collation-server = utf8_unicode_ci
    init-connect='SET collation_connection = utf8_unicode_ci'
    character-set-server = utf8

В какой конфиг это вносить, зависит от системы. В RHEL это вообще да разных конфига.

Установить саму базу данных можно скриптом `oramon_empty.sql`.

Кроме установки клиента Oracle, понадобится еще конфигурация путей к его библиотекам и конфигурация TNS (параметры соединения). Это опять же зависит от системы. У меня в RHEL7 это сделано так:

    #!/bin/bash
    LD_LIBRARY_PATH="/usr/lib/oracle/12.1/client64/lib:${LD_LIBRARY_PATH}"
    export LD_LIBRARY_PATH
    TNS_ADMIN="/etc/oracle"
    export TNS_ADMIN
    ORACLE_HOME="/usr/lib/oracle/12.1/client64/lib"
    export ORACLE_HOME 

И в /etc/oracle лежит файл tnsnames.ora со списком параметров соединений с серверами Oracle.

Перечень соединений вы также должны будете внести в базу oramon в таблицу connections. Структура таблицы такая:

    --
    -- Структура таблицы `connections`
    --
    
    DROP TABLE IF EXISTS `connections`;
    CREATE TABLE IF NOT EXISTS `connections` (
      `id` int(4) NOT NULL,
      `user` varchar(16) DEFAULT NULL,
      `passwd` varchar(16) DEFAULT NULL,
      `tns_name` varchar(32) DEFAULT NULL,
      `bookmark_type` enum('num','time') NOT NULL DEFAULT 'time',
      `bookmark_num` int(11) DEFAULT NULL,
      `bookmark_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

`user, passwd` - это параметры логона для `tns_name` указанного в файле tnsnames.ora. Разумется такой юзер должен быть создан в Oracle и у него должны быть права приконнектиться и прочитать таблицу (точнее вьюху) `DBA_AUDIT_TRAIL`.

`bookmark_type` - это способ отметки последней обработанной записи (по номеру или по времени), который будет использоваться для чтения из данной БД Oracle. А дальше, как понятно, текущая метка.  

Прямо сейчас oramon.py не проверяет параметр `bookmark_type` и считает, что записи отбираются по времени. Я оставил этот параметр на будущее. Внести записи в таблицу удобно с помощью phpMyAdmin.

В MySQL надо создать пользователя oramon с правами на базу oramon (select, isnert, update, delete). Пароль надо вписать непосредственно в файл oramon.py, в строку:

    # ---- init ----
    
    (dbhost, dbuser, dbpass, dbschema, dbcharset)=('localhost', 'oramon', 'your_oramonpass', 'oramon', 'utf8') # MySQL connection params

Это в самом начале файла. Пока так.

oramon.py не демонизирует себя. Опять же, это пока. Когда я сочту эту штуку достаточно полезной, я, возможно, вставлю демонизацию. Сейчас я использую cron.

    SHELL=/bin/bash
    PATH=/sbin:/bin:/usr/sbin:/usr/bin:/usr/lib/oracle/12.1/client64/lib
    TNS_ADMIN=/etc/oracle
    ORACLE_HOME=/usr/lib/oracle/12.1/client64/lib
    LD_LIBRARY_PATH=/usr/lib/oracle/12.1/client64/lib
    MAILTO=root
    
    # For details see man 4 crontabs
    
    # Example of job definition:
    # .---------------- minute (0 - 59)
    # |  .------------- hour (0 - 23)
    # |  |  .---------- day of month (1 - 31)
    # |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
    # |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
    # |  |  |  |  |
    # *  *  *  *  * user-name  command to be executed
    */5 * * * * oramon /home/oramon/bin/oramon.py

обратите внимание на заголовок crontab, там должны быть указаны пути необходимые для работы клиента Oracle.

И на конец, для работы с OSSIM понадобится создать в MySQL пользователя с правом коннектиться к серверу и читать вьюху `ossim_log` из БД oramon. А для OSSIM понадобится вот такой маленький плагин:

    # Alienvault plugin
    # Author: Eugene Sokolov esguardian@outlook.com
    # Plugin oramon id:9009 version:-
    # Last modification: 2016-05-04 
    # 
    # Plugin Selection Info: 
    # ESGuardian:oramon:-:n
    # 
    #
    #
    [DEFAULT]
    plugin_id=9009
    
    [config]
    type=detector
    enable=yes
    
    source=database
    source_type=mysql
    source_ip=xxx.xxx.xxx.xxx
    source_port=3306
    user=dbusername
    password=dbuserpassword
    db=oramon
    sleep=60
    
    process=
    start=no
    stop=no
    
    [start_query]
    query="SELECT id FROM ossim_log ORDER BY id DESC LIMIT 1" 
    regexp=
    [query]
    query="SELECT id,timestamp,alert_code,alert,uname,host,host_ip,ora_user,ora_role,message,raw_id,tns FROM ossim_log where id > $1 ORDER BY id"
    regexp=
    ref=0
    date={normalize_date($1)}
    plugin_sid={$2}
    src_ip={$6}
    dst_ip=
    username={$4}
    userdata1={$5}
    userdata2={$7}
    userdata3={$8}
    userdata4={$9}
    userdata5={$11}
    
    # Payload
    log={$0}, Time: {$1}, Alert_code: {$2}, Alert: {$3}, User: {$4}, Host: {$5}, Host_ip: {$6}, ORA_User: {$7}, ORA_Object {$8}, Message: {$9}, RAW_id: {$10}, TNS: {$11}

Собственно, это всё. Дальше я буду потихоньку развивать это дело.
